 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.pos.dao.custom;

import lk.ijse.pos.core.dto.OrderDetailDTO;
import lk.ijse.pos.dao.SuperDAO;

/**
 *
 * @author ranjith-suranga
 */
public interface OrderDetailDAO extends SuperDAO<OrderDetailDTO>{
    
}
