/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.pos.dao.custom.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import lk.ijse.pos.core.dto.OrderDetailDTO;
import lk.ijse.pos.dao.custom.OrderDetailDAO;
import lk.ijse.pos.dao.db.DBConnection;

/**
 *
 * @author ranjith-suranga
 */
public class OrderDetailDAOImpl implements OrderDetailDAO{

    @Override
    public boolean add(OrderDetailDTO dto) throws Exception {
        Connection connection = DBConnection.getInstance().getConnection();
        String sql = "INSERT INTO OrderDetail VALUES (?,?,?,?);";
        PreparedStatement pstm = connection.prepareStatement(sql);
        pstm.setObject(1, dto.getOrderId());
        pstm.setObject(2, dto.getItemCode());
        pstm.setObject(3, dto.getQty());
        pstm.setObject(4, dto.getUnitPrice());
        int affectedRows = pstm.executeUpdate();
        return (affectedRows > 0);
    }

    @Override
    public boolean update(OrderDetailDTO dto) throws Exception {
        Connection connection = DBConnection.getInstance().getConnection();
        String sql = "UPDATE OrderDetail SET itemCode=?, qty=?, unitPrice=? WHERE orderId=?";
        PreparedStatement pstm = connection.prepareStatement(sql);
        pstm.setObject(1, dto.getItemCode());
        pstm.setObject(2, dto.getQty());
        pstm.setObject(3, dto.getUnitPrice());
        pstm.setObject(4, dto.getOrderId());
        int affectedRows = pstm.executeUpdate();
        return (affectedRows > 0);
    }

    @Override
    public boolean delete(OrderDetailDTO dto) throws Exception {
        Connection connection = DBConnection.getInstance().getConnection();
        String sql = "DELETE FROM OrderDetail WHERE orderId = ? AND itemCode = ?";
        PreparedStatement pstm = connection.prepareStatement(sql);
        pstm.setObject(1, dto.getOrderId());
        pstm.setObject(2, dto.getItemCode());
        int affectedRows = pstm.executeUpdate();
        return (affectedRows > 0);
    }

    @Override
    public OrderDetailDTO search(OrderDetailDTO dto) throws Exception {
        Connection connection = DBConnection.getInstance().getConnection();
        String sql = "SELECT * FROM OrderDetail WHERE orderId='" + dto.getOrderId()+ "' AND itemCode='" + dto.getItemCode() + "'";
        Statement stm = connection.createStatement();
        ResultSet rst = stm.executeQuery(sql);

        if (rst.next()) {
            return new OrderDetailDTO(
                    rst.getString(1),
                    rst.getString(2),
                    rst.getInt(3),
                    rst.getBigDecimal(4));
        }

        return null;
    }

    @Override
    public ArrayList<OrderDetailDTO> getAll() throws Exception {
        Connection connection = DBConnection.getInstance().getConnection();
        String sql = "SELECT * FROM OrderDetail";
        Statement stm = connection.createStatement();
        ResultSet rst = stm.executeQuery(sql);

        ArrayList<OrderDetailDTO> alOrderDetail = null;

        while (rst.next()) {
            if (alOrderDetail == null) {
                alOrderDetail = new ArrayList<>();
            }

            alOrderDetail.add(new OrderDetailDTO(
                    rst.getString(1),
                    rst.getString(2),
                    rst.getInt(3),
                    rst.getBigDecimal(4)));

        }

        return alOrderDetail;
    }
    
}
