/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lk.ijse.pos.dao.custom.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import lk.ijse.pos.core.dto.OrderDTO;
import lk.ijse.pos.dao.custom.OrderDAO;
import lk.ijse.pos.dao.db.DBConnection;

/**
 *
 * @author ranjith-suranga
 */
public class OrderDAOImpl implements OrderDAO {

    @Override
    public boolean add(OrderDTO dto) throws Exception {
        Connection connection = DBConnection.getInstance().getConnection();
        String sql = "INSERT INTO Orders VALUES (?,?,?);";
        PreparedStatement pstm = connection.prepareStatement(sql);
        pstm.setObject(1, dto.getId());
        pstm.setObject(2, dto.getDate());
        pstm.setObject(3, dto.getCustomerId());
        int affectedRows = pstm.executeUpdate();
        return (affectedRows > 0);
    }

    @Override
    public boolean update(OrderDTO dto) throws Exception {
        Connection connection = DBConnection.getInstance().getConnection();
        String sql = "UPDATE Orders SET date=?, customerId=? WHERE id=?";
        PreparedStatement pstm = connection.prepareStatement(sql);
        pstm.setObject(1, dto.getDate());
        pstm.setObject(2, dto.getCustomerId());
        pstm.setObject(3, dto.getId());
        int affectedRows = pstm.executeUpdate();
        return (affectedRows > 0);
    }

    @Override
    public boolean delete(OrderDTO dto) throws Exception {
        Connection connection = DBConnection.getInstance().getConnection();
        String sql = "DELETE FROM Orders WHERE id = ?";
        PreparedStatement pstm = connection.prepareStatement(sql);
        pstm.setObject(1, dto.getId());
        int affectedRows = pstm.executeUpdate();
        return (affectedRows > 0);
    }

    @Override
    public OrderDTO search(OrderDTO dto) throws Exception {
        Connection connection = DBConnection.getInstance().getConnection();
        String sql = "SELECT * FROM Orders WHERE id='" + dto.getId() + "'";
        Statement stm = connection.createStatement();
        ResultSet rst = stm.executeQuery(sql);

        if (rst.next()) {
            return new OrderDTO(
                    rst.getString(1),
                    rst.getDate(2),
                    rst.getString(3));
        }

        return null;
    }

    @Override
    public ArrayList<OrderDTO> getAll() throws Exception {
        Connection connection = DBConnection.getInstance().getConnection();
        String sql = "SELECT * FROM Orders";
        Statement stm = connection.createStatement();
        ResultSet rst = stm.executeQuery(sql);

        ArrayList<OrderDTO> alOrders = null;

        while (rst.next()) {
            if (alOrders == null) {
                alOrders = new ArrayList<>();
            }

            alOrders.add(new OrderDTO(
                    rst.getString(1),
                    rst.getDate(2),
                    rst.getString(3)));

        }

        return alOrders;
    }

}
